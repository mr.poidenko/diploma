<?php
require_once 'db.php';

$sql = "SELECT * FROM subjects";
$result = $conn->query($sql);

$subjects = $result->fetch_all(MYSQLI_ASSOC);

foreach ( $subjects as $subject ) {
?>
<ul>
    <li>
        <a href="/subject/<?=$subject["slug"];?>"><?=$subject["name"];?></a>
        <ul>
            <?php

            $sql = "SELECT * FROM lection where subject_id = {$subject['id']} ";
            $result = $conn->query($sql);

            $lectures = $result->fetch_all(MYSQLI_ASSOC);

            foreach ( $lectures as $lecture ) {?>
                <li>
                    <a href="/lecture/<?=$lecture["slug"];?>"><?=$lecture["title"];?></a>
                </li>

            <?php
            }
            ?>
        </ul>
    </li>

</ul>
<?php
}
?>